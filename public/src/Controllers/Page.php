<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/15/17
 * Time: 5:55 PM
 */
declare(strict_types = 1);

namespace Example\Controllers;

use Example\Model\ItemsTodo;
use Example\Page\InvalidPageException;
use Http\Response;
use Example\Template\FrontendRenderer;
use Example\Page\PageReader;
use Http\Request;


class  Page
{
    private $response;
    private $renderer;
    private $pageReader;
    private $itemsTodo;
    private $request;

    /**
     * Page constructor.
     * @param Response $response
     * @param FrontendRenderer $renderer
     * @param PageReader $pageReader
     */
    public function __construct(
        Response $response,
        FrontendRenderer $renderer,
        PageReader $pageReader,
        ItemsTodo $itemsTodo,
        Request $request
    ) {
        $this->response = $response;
        $this->renderer = $renderer;
        $this->pageReader = $pageReader;
        $this->itemsTodo = $itemsTodo;
        $this->request = $request;

    }

    /**
     * Create content for page
     * @return mixed
     */
    public function show()
    {
        // All items for list
        $items = $this->itemsTodo->getAll();
        try {
            $data['items'] = $this->request->getParameter('items', $items);
        } catch (InvalidPageException $e) {
            $this->response->setStatusCode(404);
            return $this->response->setContent('404 - Page not found');
        }

        $html = $this->renderer->render('Page', $data);
        $this->response->setContent($html);
    }

}