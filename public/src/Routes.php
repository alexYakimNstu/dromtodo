<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/14/17
 * Time: 1:28 PM
 */
declare(strict_types=1);

return [
    ['GET', '/', ['Example\Controllers\HomePage', 'show']],
    ['GET', '/{slug}', ['Example\Controllers\Page', 'show']],

    ['POST', '/saveItem', ['Example\Model\ItemsTodo', 'save']],
    ['POST', '/deleteItem', ['Example\Model\ItemsTodo', 'deleteById']],
    ['POST', '/updateItem', ['Example\Model\ItemsTodo', 'updateById']],
];
