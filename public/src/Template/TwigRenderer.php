<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/15/17
 * Time: 7:19 PM
 */
declare(strict_types = 1);

namespace Example\Template;

use Twig_Environment;

class TwigRenderer implements Renderer
{
    private $renderer;

    public function __construct(Twig_Environment $renderer)
    {
        $this->renderer = $renderer;
    }

    public function render($template, $data = []): string
    {
        return $this->renderer->render("$template.html", $data);
    }
}