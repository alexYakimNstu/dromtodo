<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.09.17
 * Time: 16:07
 */
declare(strict_types = 1);

namespace Example;

use function Composer\Autoload\includeFile;
use Http\HttpRequest;
use Http\HttpResponse;

require __DIR__ . '/../vendor/autoload.php';
error_reporting(E_ALL);

$enviroment = 'development';

/**
 * error handler
 */
$whoops = new \Whoops\Run();
if ($enviroment !== 'production') {
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
} else {
    $whoops->pushHandler(function($e) {
        echo 'Todo: Friendly error page and send an email to the developer';
    });
}
$whoops->register();

//$request = new \Http\HttpRequest($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
//$response = new \Http\HttpResponse();

//$content = '<h1>Hello World </h1>';

$routeDefinitionCallback = function (\FastRoute\RouteCollector $r) {
    $routes = include('Routes.php');
    foreach ($routes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }
};

$dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

$injector = include('Dependencies.php');
//var_dump($injector);
//die;
/**
 * @var HttpRequest $request
 */
$request = $injector->make('Http\HttpRequest');
//var_dump($request);
//die;
/**
 * @var HttpResponse $response
 */
$response = $injector->make('Http\HttpResponse');


$routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getPath());
switch ($routeInfo[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        $response->setContent('404 - Page not found');
        $response->setStatusCode(404);
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response->setContent('405 - Method not allowed');
        $response->setStatusCode(405);
        break;
    case \FastRoute\Dispatcher::FOUND:
        $className = $routeInfo[1][0];
        $method = $routeInfo[1][1];
        $vars = $routeInfo[2];

        $class = $injector->make($className);
//        var_dump($class);
//        die;
        $class->$method($vars);
        break;
}

foreach ($response->getHeaders() as $header) {
    header($header, false);
}
echo $response->getContent();


