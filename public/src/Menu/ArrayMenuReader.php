<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/17/17
 * Time: 2:37 PM
 */
declare(strict_types = 1);

namespace Example\Menu;

class ArrayMenuReader implements MenuReader
{
    public function readMenu() : array
    {
        return [
            ['href' => '/', 'text' => 'Homepage'],
            ['href' => '/todo-list', 'text' => 'ToDo list'],
        ];
    }
}