<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/17/17
 * Time: 5:53 PM
 */
declare(strict_types = 1);

namespace Example\Model;

use Example\DbConnection\Db;

class ItemsTodo
{
    private $db;
    private $mysql;

    public function __construct()
    {
        $this->mysql = new \mysqli(
            'mysql', 'root', '123root', 'dromToDo'
        ) or die("problem");
    }

    /**
     * Return all item from list
     *
     * @return mixed
     */
    public function getAll()
    {

        $query = "SELECT * FROM todo_text ORDER BY id asc";
        $results = $this->mysql->query($query);
        return $results->fetch_all();
    }

    /**
     * Delete item from list
     *
     * @return null
     */
    public function deleteById()
    {
        if (empty($_POST['data'])) {
            return null;
        }
        $id = $_POST['data'];
        $query = "DELETE from todo_text WHERE id = $id";
        $result = $this->mysql->real_query($query) or die("There was a problem");
    }

    /**
     * Update item for list
     *
     * @return null|string
     */
    public function updateById()
    {
        if (empty($_POST['data'])) {
            return null;
        }

        $title = $_POST['data'];
        $id = $_POST['id'];

        var_dump($title);
        var_dump($id);
        die;


        $query = "UPDATE todo_text SET title = ? where  id = ? LIMIT 1";

        if ($stmt = $this->db->mysql->prepare($query)) {
            $stmt->bind_param('is', $title, $id);
            $stmt->execute();
            return "Good job;";
        }
    }

    /**
     * Save new item
     * Just for now with out check for mysql injection
     */
    public function save()
    {
        if (empty($_POST['data'])) {
            return null;
        }
        // Делай что хочешь, кому не лень :(
        $title = $_POST['data'];
        $query = "INSERT INTO todo_text (text) VALUES('$title')";

        $this->mysql->real_query($query) or die("There was a problem");
    }
}