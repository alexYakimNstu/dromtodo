<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/14/17
 * Time: 4:12 PM
 */

declare(strict_type = 1);

$injector = new \Auryn\Injector();

$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
    ':get' => $_GET,
    ':post' => $_POST,
    ':cookies' => $_COOKIE,
    ':files' => $_FILES,
    ':server' => $_SERVER,
]);

$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->alias('Example\Template\Renderer', 'Example\Template\MustacheRenderer');

$injector->alias('Example\Template\Renderer', 'Example\Template\TwigRenderer');
$injector->share('Http\HttpResponse');

$injector->define('Mustache_Engine', [
    ':option' => [
        'loader' => new Mustache_Loader_FilesystemLoader(dirname(__DIR__) . '/templates', [
            'extension' => '.html',
        ]),
    ],
]);


$injector->define('Example\Page\FilePageReader', [
    ':pageFolder' => __DIR__ . '/../pages',
]);

$injector->alias('Example\Page\PageReader', 'Example\Page\FilePageReader');
$injector->share('Example\Page\FilePageReader');

$injector->delegate('Twig_Environment', function () use ($injector) {
    $loader = new Twig_Loader_Filesystem(dirname(__DIR__) . '/templates');
    $twig = new Twig_Environment($loader);
    return $twig;
});

$injector->alias('Example\Template\FrontendRenderer', 'Example\Template\FrontendTwigRenderer');

$injector->alias('Example\Menu\MenuReader', 'Example\Menu\ArrayMenuReader');
$injector->share('Example\Menu\ArrayMenuReader');

$injector->alias('\Example\Model\ItemsTodo', '\Example\Model\ItemsTodo');
$injector->share('\Example\Model\ItemsTodo');

$injector->share('Example\Template\FrontendTwigRenderer');
return $injector;