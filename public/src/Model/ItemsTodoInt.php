<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/17/17
 * Time: 6:39 PM
 */
declare(strict_types = 1);

namespace ItemsTodo\Model;

interface ItemsTodoInt
{
    public function getAll() : array;
}