<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/15/17
 * Time: 6:01 PM
 */
declare(strict_types = 1);

namespace Example\Page;

interface PageReader
{
    public function readBySlug(string $slug) : string;
}