(function (window) {
    'use strict';
    $(".new-todo").keypress(function (e) {
        if (e.which == 13) {

            var item = $(".new-todo").val();

            $.ajax({
                url: '/saveItem',
                type: 'POST',
                data: {action: "save", data: item},
                success: function (response) {
                    //Тут должна подставляться новая строка
                }
            });
        }
    });

    $(".toggle").on('click', $.proxy(function () {
        
        console.log('Done with item');
        return null
    }));


    $(".destroy").on('click', $.proxy(function () {

        //!!! Берет первый id из списка!!! Разобраться
        var id = $(".toggle").val();

        $.ajax({
            url: '/deleteItem',
            type: 'POST',
            data: {action: "delete", data: id},
            success: function (response) {
                //Тут должна удаляться ненужная строка
            }
        });
    }));

    $(".view").on( 'dblclick', $.proxy(function () {

        console.log('Update this item');
        return null;
        // var id = $(".toggle").val();
        // var data = $(".toggle").val();
        // $.ajax({
        //     url: '/updateItem',
        //     type: 'POST',
        //     data: {action: "updateById", id: 'id' ,data: 'data'},
        //     success: function (response) {
        //         //Тут должна удаляться ненужная строка
        //     }
        // });

    }));


})(window);
