<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/17/17
 * Time: 5:44 PM
 */
namespace Example\DbConnection;

class Db
{
    public $mysql;

    public function getConnection()
    {
        $this->mysql = new \mysqli(
            'localhost', 'root', '123', 'todo'
        ) or die("problem");
    }
}