<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/14/17
 * Time: 5:01 PM
 */
declare(strict_types = 1);

namespace Example\Template;

interface Renderer
{
    public function render($template, $data = []) : string;
}