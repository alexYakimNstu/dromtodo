<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/17/17
 * Time: 2:36 PM
 */
declare(strict_types = 1);

namespace Example\Menu;

interface MenuReader
{
    public function readMenu() : array;
}