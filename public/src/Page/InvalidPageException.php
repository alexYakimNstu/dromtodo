<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9/15/17
 * Time: 6:25 PM
 */
declare(strict_types = 1);

namespace Example\Page;

use Exception;

class InvalidPageException extends Exception
{
    public function __construct($slug = "", $code = 0, Exception $previous = null)
    {
        $message = "No page with the `$slug` was found";
        parent::__construct($message, $code, $previous);
    }
}