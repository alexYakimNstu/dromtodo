SET NAMES utf8;

CREATE TABLE todo_text (
  id int(10) NOT NULL auto_increment,
  text VARCHAR (1000) NOT NULL,
  PRIMARY KEY(id)
  ) Default charset=utf8;